// =============================================================================
// CGL - Using Extensions
// =============================================================================
// Description:
// Example using the CGL Extensions for PNG Loading and MP3 Streaming
//
// =============================================================================
// This program is free software; you can redistribute it and use it under the terms of the
// CGL License. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// =============================================================================
#include <stdio.h>
#include <cgl/cgl.h>
#include <core/corefile.h>
#include <core/core.h>
#include <coresound/coresound.h>
#include <extensions/coresound_mp3.h> // CGL Extension - MP3 Streaming
#include <extensions/corepng.h> // CGL Extension - PNG Loader

// ======================== variables ===============================
s_bitmap bmp_background;
s_sprite spr_player;
s_sound snd_gamemusic;
s_font myfont;

int playerx,playery;
int exitgame;


// ======================== functions ===============================
void HandleInput()

{
    int key,keytrig;

    CGL_GetKeys(&key,&keytrig);
    if (key & CGL_INPUT_KEY_LEFT) playerx--;
    if (key & CGL_INPUT_KEY_RIGHT) playerx++;
    if (key & CGL_INPUT_KEY_UP) playery--;
    if (key & CGL_INPUT_KEY_DOWN) playery++;
    if (key & CGL_INPUT_KEY_EXIT) exitgame=1;
}


// ===================== Main - Program entry =================
int coremain(int argc, char* args[])

{
    float currentframe,framestep;
    int framebuffer_id;

    // Initialise resources
    corefile_mountimage("res",MOUNT_DIR);
    
	// Initialise videomode
	CGL_SetTitle("CGL - Using Extensions");
    CGL_InitVideo(640,480,CGL_VIDEO_NONE);
    Soundsystem_Init(KHZ_44,16,CORESOUND_STEREO);

    // Extension MP3CGL - Stream MP3 files
    Soundsystem_InitMP3Stream("minuit.mp3",&snd_gamemusic);
    Soundsystem_PlaySound(&snd_gamemusic,1);

    // Load background image (PNG), sprite and font
    CGL_LoadBitmapPNG("planet640.png",&bmp_background);
    CGL_LoadSprite("ball.tga",&spr_player);
    CGL_LoadFont("font_heat.tga",&myfont);

    // update loop
    playerx=100;playery=120;
    currentframe=0;
    exitgame=0;
    do {
      framestep=CGL_WaitRefresh();
      CGL_DrawBitmap(0,0,bmp_background);
      CGL_DrawSprite(playerx,playery,spr_player);
      CGL_DrawText(10,10,myfont ,"CGL - Using Extensions");

      HandleInput();

      CGL_SwapBuffers();
      currentframe+=framestep;
    } while(exitgame==0);
    Soundsystem_StopSound(&snd_gamemusic);
    Soundsystem_DeleteSound(&snd_gamemusic);
}


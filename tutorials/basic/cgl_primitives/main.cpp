// =============================================================================
// CGL - Minimal Example
// =============================================================================
// Description:
// Basic Example of setting up the CGL system and displaying graphics.
// - Initialising the corefile system
// - Initialising videomode
// - Creating and selecting a FrameBuffer to draw on
// - Loading and Drawing Bitmaps, Sprites and Texts
// - Simple input handling
//
// =============================================================================
// This program is free software; you can redistribute it and use it under the terms of the
// CGL License. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// =============================================================================
#include <cgl/cgl.h>
#include <core/corefile.h>

// ======================== variables ===============================
s_bitmap bmp_background;
s_sprite spr_player;
s_font myfont;

int playerx,playery;
int exitgame;

// ======================== functions ===============================
void HandleInput()

{
    int key,keytrig;

    CGL_GetKeys(&key,&keytrig);
    if (key & CGL_INPUT_KEY_LEFT) playerx-=2;
    if (key & CGL_INPUT_KEY_RIGHT) playerx+=2;
    if (key & CGL_INPUT_KEY_UP) playery-=2;
    if (key & CGL_INPUT_KEY_DOWN) playery+=2;
    if (key & CGL_INPUT_KEY_EXIT) exitgame=1;
}


// ===================== Main - Program entry =================
int coremain(int argc, char* args[])

{
    float currentframe,framestep;
    int framebuffer_id;

    // Initialise resources
    corefile_mountimage("res",MOUNT_DIR);

	// Initialise videomode
    CGL_SetTitle("CGL - Using Primitives");
    CGL_InitVideo(640,480,CGL_VIDEO_NONE);

	// Create a framebuffer to draw onto
    framebuffer_id = CGL_CreateFrameBuffer(640,480,CGL_FRAMEBUFFER_DEFAULT);
    CGL_SetFrameBuffer(framebuffer_id);

    // Load background image, sprite and font
    CGL_LoadBitmap("planet640.tga",&bmp_background);
    CGL_LoadSprite("ball.tga",&spr_player);
    CGL_LoadFont("font_heat.tga",&myfont);

    // update loop
    playerx=100;playery=120;
    currentframe=0;
    exitgame=0;
    do {
      framestep=CGL_WaitRefresh();
      CGL_DrawBitmap(0,0,bmp_background);
      CGL_DrawSprite(playerx,playery,spr_player);
      //CGL_DrawBox(20,20,320,240,0xffff0000);
      //CGL_DrawLine(100,0,400,480,0xff0000ff);
      CGL_DrawBox(20,20,320,240,0xff);
      CGL_DrawLine((int)currentframe%640,10,600-(int)currentframe%640,440,0xff0000);

      CGL_DrawText(10,10,myfont ,"CGL - Using Primitives");
      HandleInput();

      CGL_SwapBuffers();
      currentframe+=framestep;
    } while(exitgame==0);
}


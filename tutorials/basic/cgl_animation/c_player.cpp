#include <cgl/cgl.h>
#include "c_player.h"

void c_player::Init(int startx,int starty)

{
    x=startx;y=starty;
    dx=0;dy=0;
    score=0;lives=3;
    direction=1;
    currentanim=0;

    // Init animations
    anim[ANIM_STILL].startframe=0;anim[ANIM_STILL].endframe=1;anim[ANIM_STILL].speed=8;anim[ANIM_STILL].loop=1;
    anim[ANIM_WALK].startframe=2;anim[ANIM_WALK].endframe=5;anim[ANIM_WALK].speed=4;anim[ANIM_WALK].loop=1;
    anim[ANIM_JUMP].startframe=6;anim[ANIM_JUMP].endframe=6;anim[ANIM_JUMP].speed=0;anim[ANIM_JUMP].loop=0;
}

void c_player::InitAnim(int animnr)

{
    if (animnr!=currentanim) {
       animframe=anim[animnr].startframe;
       currentanim=animnr;
       animcount=0;
    }
}

void c_player::Draw()

{
    // Draw player sprite
    if (direction==1) CGL_DrawSprite(x,y,spr_player[animframe] );
       else CGL_DrawMirroredSprite(x,y,spr_player[animframe] );

    // Handle animation
    animcount++;
    if (animcount>anim[currentanim].speed) {
       animframe++;
       if (animframe>anim[currentanim].endframe) {
          if (anim[currentanim].loop) animframe=anim[currentanim].startframe;
            else animframe=anim[currentanim].endframe;
       }
       animcount=0;
    }
}


void c_player::Handle()

{
    int key,keytrig;

    // Handle Input
    CGL_GetKeys(&key,&keytrig);
    if (key==0) InitAnim(ANIM_STILL);
    if (key & CGL_INPUT_KEY_LEFT) {
       InitAnim(ANIM_WALK);
       dx-=0.1;direction=0;
    }
    if (key & CGL_INPUT_KEY_RIGHT) {
       InitAnim(ANIM_WALK);
       dx+=0.1;direction=1;
    }
    if ((key & CGL_INPUT_KEY_UP) && (dy>=0)) {
       InitAnim(ANIM_JUMP);
       dy=-3;
    }
    if (key & CGL_INPUT_KEY_EXIT) exitgame=1;

    // Move player and apply gravity and friction
    x+=dx;y+=dy;
    dx*=0.95;dy+=0.1;
    if (y>160) dy=0;

    // Clip to screen edges
    if (x<0) dx=-dx;
    if (x>300) dx=-dx;
}


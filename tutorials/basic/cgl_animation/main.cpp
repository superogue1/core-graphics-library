// =============================================================================
// CGL - Working with Animations
// =============================================================================
// Description:
// Working with player animations
//
// - Grabbing an array of sprites from a spritesheet
// - Player class with gravity and friction movement
// - Simple player statemachine for generic handling of animations
//
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the CGL License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// =============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <cgl/cgl.h>
#include <core/core.h>
#include <core/corefile.h>
#include <coresound/coresound.h>
#include "c_player.h"

// ======================== variables ===============================
s_bitmap bmp_background;
s_sprite spr_player[MAXFRAMES];
s_font myfont;

c_player player;
int exitgame;

// ======================== init functions ===============================
void LoadGraphics()

{
    s_bitmap spritebmp;

    // Load background image
    CGL_LoadBitmap("ffbg.tga",&bmp_background);
    // Get sprites from page
    CGL_LoadBitmap("playersprite.tga",&spritebmp);
    for (int i=0;i<MAXFRAMES;i++) {
        CGL_GetSprite(i*32,0,32,32,spritebmp,&spr_player[i]);
    }
    CGL_LoadFont("font_heat.tga",&myfont);
}



// ===================== Main - Program entry =================
int coremain(int argc, char* args[])

{
    float currentframe,framestep;
    int framebuffer_id;

    CGL_SetTitle("CGL - Animation example");

    // initialise resources map and videomode
    corefile_mountimage("res",MOUNT_DIR);
    CGL_InitVideo(640,480,CGL_VIDEO_NONE);

    framebuffer_id=CGL_CreateFrameBuffer(320,240,CGL_FRAMEBUFFER_DEFAULT);
    CGL_SetFrameBuffer(framebuffer_id);

    LoadGraphics();
    player.Init(160,160);

    // update loop
    currentframe=0;
    exitgame=0;
    do {
      framestep=CGL_WaitRefresh();
      CGL_DrawBitmap(0,0,bmp_background);
      player.Draw();
      CGL_DrawText(10,10,myfont ,"Animation example");

      player.Handle();
      CGL_SwapBuffers();
      currentframe+=framestep;
    } while(exitgame==0);
}


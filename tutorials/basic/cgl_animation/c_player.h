#ifndef __C_PLAYER_H__
#define __C_PLAYER_H__

#include <cgl/cgl.h>

#define MAXANIMS 5
#define MAXFRAMES 8

#define ANIM_STILL 0
#define ANIM_WALK  1
#define ANIM_JUMP  2
#define ANIM_HIT   3
#define ANIM_KICK  4

extern s_sprite spr_player[MAXFRAMES];
extern int exitgame;

typedef struct {
        int startframe,endframe,speed,loop;
        } s_anim;

class c_player {
    public:
    float x,y;
    float dx,dy;
    int score;
    int lives;
    int direction;
    // animation
    s_anim anim[MAXANIMS];
    int currentanim;
    int animframe,animcount,animloop;
    int startframe,endframe,animspeed;

    void Init(int startx,int starty);
    void InitAnim(int animnr);
    void Draw();
    void Handle();
};


#endif

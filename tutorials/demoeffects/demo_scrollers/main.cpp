// =============================================================================
// CGL - Robot Scroller demo
// =============================================================================
// Description:
// Small demo displaying 8 different scrollers using the CGL font routines.
//
// - Loading Resources from a datafile
// - Using the Coresound system to play a musicloop
// - Creating a scroller effect
//
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the CGL License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// =============================================================================
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <core/core.h>
#include <core/corefile.h>
#include <coresound/coresound.h>
#include <cgl/cgl.h>

const char scrolltext[]="                                                                   " \
"Revival studios is a game-development studio specialised in game development for classic game systems. "\
"We develop high quality products on a wide range classic platforms, ranging from the old consoles like the vectrex and colecovision, forgotten handheld systems such as the NeoGeoPocket and Bandai Wonderswan, " \
"to newer consoles like the playstation, n64, psp and sega dreamcast.... while we might not have immediate products launch for every platform. Here is a full list of platforms we are currently developing for: " \
"Atari 2600, Colecovision, Vectrex, Videopac, Gameboy/Gameboy Color, MSX, Atari Lynx, Atari Jaguar, Neo Geo CD, Sega Mastersystem, Sega Megadrive/Genesis, Sega CD, " \
"Sega Saturn, Sega Dreamcast, Gameboy Advance, Nintendo Gamecube, Nintendo DS, Nintendo N64, Xbox, Win32, PDA, Gamepark GP2X, Sony PS1, Sony PS2 and Sony PSP... " \
"We might even add more systems in the future ;-)   We've currently have products ready for release on Vectrex, Sega Dreamcast and GP2X, but we will have more products ready for the Atari 2600, Colecovision, MSX and Videopac very soon... " \
"For the latest updates check out our website at http://www.revival-studios.com                                               ";

char scrollbuffer[100];
int scrollcount[8],scrollspeed[8],scrollx[8],scrolly[8],scrollofs[8];
s_font *scrollfont[8];
int sintab[256];
int scrolllength;

s_font font1,font2,font3,font4,font5,font6,font7;
s_bitmap imagebmp,effectbmp;
s_sprite robotspr;
s_sound demomusic;
float currentframe,framestep;


void InitScrollers()

{
    // init scrollers
    scrolllength=strlen(scrolltext)-100;
    for (int i=0;i<8;i++) {
        scrollspeed[i]=(i*8)+8;
        scrollcount[i]=0;
        scrollx[i]=0;
        scrolly[i]=10+(i*24);
        scrollofs[i]=0;
    }
    scrollbuffer[99]=0;
    // set fonts to different scrollers
    scrollfont[0]=&font1;
    scrollfont[1]=&font2;
    scrollfont[2]=&font3;
    scrollfont[3]=&font4;
    scrollfont[4]=&font5;
    scrollfont[5]=&font6;
    scrollfont[6]=&font7;
    scrollfont[7]=&font2;

    // generate sinus table
    for (int i=0;i<256;i++) {
        sintab[i]=(int)(sin(M_PI/128*i)*64);
    }
}

void HandleScrollers()

{
    int pos,scrollsy;
    char c;

    // handle 8 scrollers
    for (int i=0;i<8;i++) {
        // copy scrolltext
        pos=scrollofs[i];
        for (int j=0;j<99;j++) scrollbuffer[j]=scrolltext[pos+j];

        // draw scroller
        scrollsy=sintab[((int)currentframe+pos+(i<<3)) & 255]+scrolly[i];
        CGL_DrawText(-scrollx[i],scrollsy,*scrollfont[i] ,scrollbuffer);

        // update scroller
        scrollcount[i]+=(int)(scrollspeed[i]*framestep);
        if (scrollcount[i]>=32) {
           c=scrolltext[scrollofs[i]];
           scrollx[i]++;
           if (scrollx[i]>=scrollfont[i]->fontwidth[c-31]) {
              scrollofs[i]++;
              if (scrollofs[i]>scrolllength) scrollofs[i]=0;
              scrollx[i]=0;
           }
           scrollcount[i]-=32;
        }
    }
}


// ============================= MAIN PROGRAM =====================
int coremain(int argc, char* args[])
{
    int key,keytrig;
    int framebuffer_id;
    int exit;

    corefile_mountimage("res.dat",MOUNT_FILE);
    CGL_SetTitle("CGL - Robot Scroller demo");
    CGL_InitVideo(640,480,CGL_VIDEO_NONE);

    // Create FrameBuffer of 320x240 (will automatically be scaled up)
    framebuffer_id=CGL_CreateFrameBuffer(320,240);
    CGL_SetFrameBuffer(framebuffer_id);

    dbg_printf("- Init fonts\n");
    CGL_LoadFont("font_arial.tga",&font1);
    CGL_LoadFont("font_small01.tga",&font2);
    CGL_LoadFont("font_small06.tga",&font3);
    CGL_LoadFont("font_small07.tga",&font4);
    CGL_LoadFont("font_micro.tga",&font5);
    CGL_LoadFont("font_heat.tga",&font6);
    CGL_LoadFont("font_design.tga",&font7);

    dbg_printf("- load backgrounds\n");
    CGL_LoadBitmap("robotbg.tga",&imagebmp);
    CGL_LoadSprite("robot.tga",&robotspr);


    dbg_printf("- Init Soundsystem\n");
    Soundsystem_Init(KHZ_44,16,CORESOUND_STEREO);
    Soundsystem_SetMasterVolume(256);

    dbg_printf("- Loading sounds\n");
    Soundsystem_LoadSound("music.wav",&demomusic);

    // main loop
    dbg_printf("- Play background music\n");
    Soundsystem_PlaySound(&demomusic,1);

    InitScrollers();
    exit=0;
    do {
       framestep=CGL_WaitRefresh();
       CGL_DrawBitmap(0,0,imagebmp);
       HandleScrollers();
       CGL_DrawSprite(0,0,robotspr);
       CGL_SwapBuffers();

       CGL_GetKeys(&key, &keytrig);
       if (key & CGL_INPUT_KEY_START) { exit=1; }
       if (key & CGL_INPUT_KEY_EXIT) { exit=1; }
       currentframe+=framestep;
    } while (!exit);
    dbg_printf("Cleaning up resources!\n");
    Soundsystem_StopSound(&demomusic);
    Soundsystem_DeleteSound(&demomusic);
    dbg_printf("Shutting down!\n");
}

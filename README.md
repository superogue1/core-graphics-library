# Core Graphics Library
The Core Graphics Library (CGL) is a lightweight 2D/3D abstraction layer that allows quick and easy access to Graphics, sound and input, while providing for an easier to use interface as well as more accessible features (such as rotated/scaled sprites, blending, etc. Flexible soundsystem,etc.) out of the box.

The Core Graphics library is developed as a hardware-accelerated successor to the older (software-only) CML Library. 
The latest version is in development is version 1.4b.


Modules:

- Core system (#include <core/core.h>) - Core functionality for memory and debugging
- Corefile system (#include <core/corefile.h>) - Core file system (loading from disk and datafile)
- Core Soundsystem (#include <coresound/coresound.h>) - The Soundsystem
- CGL (#include <cgl/cgl.h>) - Core Graphics Library (Handling of Bitmaps,Sprites,Fonts,Primitives,FrameBuffers,etc.)


Extensions:

- corefont_ft2 - FreeType2 font implementation for CGL (Allows use of .ttf truetype fonts). 
- coresound_mp3 - MP3 file streaming
- cgl_loadjpeg - Support for JPEG file loading
- corevideoplayer - Simple Video compression/format support
- resbrowser - Browse your resources


Platform Support:

-    Windows (Direct Platform Support via Windows/OpenGL, SDL/SDL2)
-    Linux (via SDL/SDL2/OpenGL).
-    Mac OSX (via SDL/SDL2/OpenGL)


Available Tools:

- CoreExport.ms - Core 3D Mesh exporter for 3D Studio Max
- Font Tool (Windows) - Convert Windows fonts to a BMP file to be used with CGL
- CMV_VideoConverter (Windows) - Convert AVI files to CoreMotionVideo format.


Tutorials:

There are many tutorials available in different categories.

- Basic - Basic usage of the library and extensions
- Games - Various example games
- Demo Effects - Various example demoeffects

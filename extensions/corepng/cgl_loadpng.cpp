#include <stdio.h>
#include <stdbool.h>
#include <core/core.h>
#include <core/corefile.h>
#include <cgl/cgl.h>
#include "loadpng.h"

void CGL_LoadBitmapPNG(char *fn,s_bitmap *image)
{
  s_corefile infile;
  unsigned char *pngbuffer;
  int c;

  corefile_open(fn,&infile,FILE_READ);
  int pngsize=corefile_fsize(&infile);
  corefile_close(&infile);
  pngbuffer=(unsigned char*)wrap_malloc(pngsize);
  pngbuffer=corefile_loadbuffer(fn);
    printf("pngbuffer loaded: %d",pngsize);
    unsigned char *sourceData;
    unsigned int sourceWidth, sourceHeight;
    unsigned int error = lodepng_decode32(&sourceData, &sourceWidth, &sourceHeight, pngbuffer,pngsize);
    if (error != 0) {
        // error
        printf("Error loading image: %s\n",lodepng_error_text(error));
    } else {
        printf("PNG width: %d,height:%d\n",sourceWidth,sourceHeight);
        CGL_CreateBitmap(sourceWidth,sourceHeight,image);

        for (int y=0;y<sourceHeight;y++) {
            for (int x=0;x<sourceWidth;x++) {
                c=(sourceData[(y*sourceWidth+x)*4+3] << 24)+
                  (sourceData[(y*sourceWidth+x)*4+0] << 16)+
                  (sourceData[(y*sourceWidth+x)*4+1] << 8) +
                  (sourceData[(y*sourceWidth+x)*4+2]);
                CGL_DrawPixel(x,y,c,*image);
            }
        }
        CGL_UpdateBitmap(*image);
    }
}

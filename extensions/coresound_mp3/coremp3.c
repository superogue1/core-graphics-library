#include <stdio.h>
#include <stdbool.h>
#include <core/core.h>
#include <core/corefile.h>
#include <coresound/coresound.h>
#include "minimp3.h"

static mp3_decoder_t mp3;
static mp3_info_t mp3info;
static unsigned char *mp3stream,*mp3_streampos;
static int mp3_size,mp3_framesize,mp3_bytesleft;
int mp3_looping;

s_corefile mp3file;
unsigned char *mp3buf;
char mp3_filename[256];
s_sound *mp3_sound;
int mp3ret;

int MP3_callback(void* buf, unsigned int length);


void ShowTag(const char *caption, const unsigned char *src, int max_length) {

    static char tagbuf[32];
    char *tagpos = tagbuf;
    tagbuf[max_length] = '\0';
    wrap_memcpy(tagpos,(unsigned char*)src,max_length);
    if (!*tagbuf) return;
    dbg_printf("%s - %s\n",caption,tagbuf);
}


void Soundsystem_InitMP3Stream(char *fn,s_sound *sound)

{
	signed short *sample_buffer;
	unsigned char *chunkdata;
	int size,ret;
	char *inptr;

	// open chunk
#ifdef USE_STDIO
	FILE *f;
	f=fopen(fn,"rb");
	fseek(f, 0L, SEEK_END);size=ftell(f);fseek(f, 0L, SEEK_SET);
	mp3stream=(unsigned char*)wrap_malloc(size);
	fread(mp3streem,chunksize,1,f);
	fclose(f);
#else
	ret = corefile_open(fn,&mp3file,FILE_READ);
	size=corefile_fsize(&mp3file);
	mp3stream=(unsigned char*)wrap_malloc(size);
	corefile_read(mp3stream,size,1,&mp3file);
	corefile_close(&mp3file);
#endif
	mp3_size=size;
	mp3_streampos=mp3stream;
	mp3_bytesleft=mp3_size-128;
	mp3_looping=1;
	mp3_sound=sound;
	strcpy(mp3_filename,fn);

    // check for a ID3 tag
    inptr = mp3_streampos + mp3_bytesleft;
    if (((*(unsigned long *)inptr) & 0xFFFFFF) == 0x474154) {
        ShowTag("\nTitle: ",   inptr +  3, 30);
        ShowTag("\nArtist: ",  inptr + 33, 30);
        ShowTag("\nAlbum: ",   inptr + 63, 30);
        ShowTag("\nYear: ",    inptr + 93,  4);
        ShowTag("\nComment: ", inptr + 97, 30);
    }

    // set up minimp3 and decode the first frame
    mp3 = mp3_create();
    sample_buffer=(signed short*)wrap_malloc(MP3_MAX_SAMPLES_PER_FRAME*2);
    mp3_framesize=mp3_decode(mp3,mp3_streampos,mp3_bytesleft,sample_buffer,&mp3info);
    if (!mp3_framesize) {
        dbg_printf("\nError: not a valid MP3 audio file!\n");
    }
    dbg_printf("samplerate: %d , channels: %d, audiobytes: %d\n",mp3info.sample_rate,mp3info.channels,mp3info.audio_bytes);
    wrap_free(sample_buffer);
    Soundsystem_CreateSoundStream(sound,(void *)MP3_callback,mp3info.sample_rate,16,mp3info.channels,(mp3info.audio_bytes/mp3info.channels));

}

void Soundsystem_CloseMP3Stream()

{
wrap_free(mp3stream);
}

int MP3_callback(void* buf, unsigned int length)
{
int size,len;

//   dbg_printf("MP3callback\n");
   len=length;
   if (mp3_bytesleft>0) {
      mp3_framesize=mp3_decode(mp3, mp3_streampos, mp3_bytesleft, (signed short *)buf, &mp3info);
      mp3_streampos+=mp3_framesize;
      mp3_bytesleft-=mp3_framesize;
   } else {
      if (mp3_looping) {
         dbg_printf("looping mp3\n");
		 mp3_streampos=mp3stream;
		 mp3_bytesleft=mp3_size-128;
         /*
         Soundsystem_CloseMP3Stream();
         Soundsystem_InitMP3Stream(mp3_filename,mp3_sound);
         Soundsystem_PlaySound(&mp3_sound,0);
      	*/
	  }
   }
  // dbg_printf("framesize: %d, bytesize: %d\n",mp3_framesize,mp3info.audio_bytes);
   return mp3info.audio_bytes;
}



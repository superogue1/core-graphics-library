// =============================================================================
#ifndef __COREINPUT_H__
#define __COREINPUT_H__


// Input/Controllers
#define CGL_MAXCONTROLLERS              	8
#define CGL_MAXCONTROLLERKEYS 				16
#define CGL_CONTROLLERTYPE_NONE               0x0000
#define CGL_CONTROLLERTYPE_DIGITALCONTROLLER  0x0001
#define CGL_CONTROLLERTYPE_ANALOGUECONTROLLER 0x0002
#define CGL_CONTROLLERTYPE_KEYBOARD           0x0003
#define CGL_CONTROLLERTYPE_MOUSE              0x0004
#define CGL_CONTROLLERTYPE_LIGHTGUN           0x0005
#define CGL_CONTROLLER_DEADZONE               64


#define CGL_INPUT_KEY_NONE		0x0000
#define CGL_INPUT_KEY_UP		0x0001
#define CGL_INPUT_KEY_DOWN		0x0002
#define CGL_INPUT_KEY_LEFT		0x0004
#define CGL_INPUT_KEY_RIGHT		0x0008
#define CGL_INPUT_KEY_BUTTONA		0x0010
#define CGL_INPUT_KEY_BUTTONB		0x0020
#define CGL_INPUT_KEY_BUTTONC		0x0040
#define CGL_INPUT_KEY_BUTTOND		0x0080
#define CGL_INPUT_KEY_BUTTONL1		0x0100
#define CGL_INPUT_KEY_BUTTONL2		0x0200
#define CGL_INPUT_KEY_BUTTONR1		0x0400
#define CGL_INPUT_KEY_BUTTONR2		0x0800
#define CGL_INPUT_KEY_START		    0x1000
#define CGL_INPUT_KEY_SELECT		0x2000
#define CGL_INPUT_KEY_SPECIAL		0x4000
#define CGL_INPUT_KEY_EXIT		    0x8000

#define CGL_INPUT_KEYCODE_ESCAPE	0xF000
#define CGL_INPUT_KEYCODE_UP		0xF001
#define CGL_INPUT_KEYCODE_DOWN		0xF002
#define CGL_INPUT_KEYCODE_LEFT		0xF003
#define CGL_INPUT_KEYCODE_RIGHT		0xF004
#define CGL_INPUT_KEYCODE_ENTER		0xF010
#define CGL_INPUT_KEYCODE_SHIFT		0xF011
#define CGL_INPUT_KEYCODE_SPACE		0xF012
#define CGL_INPUT_KEYCODE_CONTROL	0xF013
#define CGL_INPUT_KEYCODE_ALT	    0xF014
#define CGL_INPUT_KEYCODE_BACKSPACE	0xF020
#define CGL_INPUT_KEYCODE_INSERT	0xF021
#define CGL_INPUT_KEYCODE_DELETE	0xF022
#define CGL_INPUT_KEYCODE_PAGEUP    0xF023
#define CGL_INPUT_KEYCODE_PAGEDOWN  0xF024
#define CGL_INPUT_KEYCODE_HOME	    0xF025
#define CGL_INPUT_KEYCODE_END   	0xF026
#define CGL_INPUT_KEYCODE_CAPSLOCK	0xF027
#define CGL_INPUT_KEYCODE_TAB	    0xF028
#define CGL_INPUT_KEYCODE_F1	    0xF030
#define CGL_INPUT_KEYCODE_F2	    0xF031
#define CGL_INPUT_KEYCODE_F3	    0xF032
#define CGL_INPUT_KEYCODE_F4	    0xF033
#define CGL_INPUT_KEYCODE_F5	    0xF034
#define CGL_INPUT_KEYCODE_F6	    0xF035
#define CGL_INPUT_KEYCODE_F7	    0xF036
#define CGL_INPUT_KEYCODE_F8	    0xF037
#define CGL_INPUT_KEYCODE_F9	    0xF038
#define CGL_INPUT_KEYCODE_F10	    0xF039
#define CGL_INPUT_KEYCODE_F11	    0xF03A
#define CGL_INPUT_KEYCODE_F12	    0xF03B

#define CGL_INPUT_MOUSEMODE_RELATIVE    0
#define CGL_INPUT_MOUSEMODE_ABSOLUTE    1
#define CGL_INPUT_MOUSEMODE_DELTA       2

#define CGL_INPUT_MOUSEBUTTON_NONE	    0x0000
#define CGL_INPUT_MOUSEBUTTON_LEFT	    0x0001
#define CGL_INPUT_MOUSEBUTTON_RIGHT	    0x0002
#define CGL_INPUT_MOUSEBUTTON_MIDDLE 	0x0004

typedef struct {
    int type,id;
    int x,y,x2,y2;
    int buttons;
	int buttonstrig;
	int connected;
    } s_CGLcontroller;

extern s_CGLcontroller CGL_controller[CGL_MAXCONTROLLERS];
extern int CGL_keys[256],CGL_numcontrollers;

void CGL_InitControllers();
void CGL_PollControllers();
void CGL_AddKeyController(int *keylist);
void CGL_SetKeyController(int controllernumber,int *keylist);
void CGL_GetControllerKeys(int id,int *key,int *keytrig);
void CGL_GetAllControllers(int *key,int *keytrig);

void CGL_GetKeycode(int *key, int *keytrig);
void CGL_GetKeys(int *key, int *keytrig);
void CGL_GetMouse(int *x, int *y, int *buttons);
void CGL_GetMouseWheel(int *z,int *zdir);
void CGL_SetMouse(int x, int y);
void CGL_SetMouseMode(int mode);
void CGL_ShowMouse();
void CGL_HideMouse();

#endif

#ifndef __COREVIDEO_H__
#define __COREVIDEO_H__

// Video mode flags
#define CGL_VIDEO_NONE		    0x00000
#define CGL_VIDEO_FULLSCREEN    0x00001
#define CGL_VIDEO_CENTER        0x00002
#define CGL_VIDEO_RESCALE       0x00004
#define CGL_VIDEO_ACCELERATED   0x00008

#define CGL_VALUETYPE_FLOAT		0
#define CGL_VALUETYPE_INT		1

#define CGL_BLITTERMODE_NONE     0x00000
#define CGL_BLITTERMODE_STD32    0x00000
#define CGL_BLITTERMODE_STD16    0x00010

#define CGL_MAXFRAMEBUFFERS	16

// Render States
#define CGL_PROJECTIONMODE_2D	0
#define CGL_PROJECTIONMODE_3D	1

#define CGL_RENDERSTATE_DEPTHTEST	1
#define CGL_RENDERSTATE_ALPHATEST	2
#define CGL_RENDERSTATE_BLEND		4
#define CGL_RENDERSTATE_LIGHTING	8

#define CGL_FILTERMODE_NONE         0
#define CGL_FILTERMODE_BILINEAR     1
#define CGL_FILTERMODE_BLUR         2
#define CGL_FILTERMODE_ANTIALIAS    3

#define CGL_BLENDMODE_NONE      0
#define CGL_BLENDMODE_MIX       1
#define CGL_BLENDMODE_ADD       2
#define CGL_BLENDMODE_MUL       3
#define CGL_BLENDMODE_MIX25     4
#define CGL_BLENDMODE_MIX75     5
#define CGL_BLENDMODE_DARKEN    6
#define CGL_BLENDMODE_BRIGHTEN  7
#define CGL_BLENDMODE_COLOR     8
#define CGL_BLENDMODE_INVERT    9

typedef struct {
	int width,height;
	int id;
	} s_framebuffer;

typedef struct {
	int *pixeldata;
	unsigned int fbo,fbo_texture,fbo_depthtexture,rbo_depth;
	} s_framebufferdata;

extern s_framebufferdata CGL_framebufferdata[CGL_MAXFRAMEBUFFERS];
extern s_framebuffer CGL_framebuffers[CGL_MAXFRAMEBUFFERS];
extern s_framebuffer *CGL_FrameBuffer;
extern int CGL_numframebuffers;
extern int CGL_numshaders;
extern int CGL_shaders_enabled;

// Standard Video Functionality
void CGL_InitVideo(int width,int height,int flags);
void CGL_CloseVideo();
float CGL_WaitRefresh();
void CGL_SwapBuffers();
void CGL_SwapBuffersBasic();
void CGL_Perspective(float fovy, float aspect, float zNear, float zFar);

// Default State settings
void CGL_EnableRenderState(int renderstate);
void CGL_DisableRenderState(int renderstate);
void CGL_SetBlendMode(int blendmode);
void CGL_EnableAlphaBlend(float alpha);
void CGL_DisableAlphaBlend();
void CGL_Enable2D();
void CGL_Disable2D();

// Frame Buffers
int CGL_CreateFrameBuffer(int width,int height);
void CGL_SetFrameBuffer(int fbid);
void CGL_DrawFrameBuffer(int sx,int sy,int width,int height,int id);
void CGL_ClearFrameBuffer(int color);

// Textures
int CGL_CreateTexture(int width,int height,void *data,int flags);
void CGL_UpdateTexture(int textureid,int width,int height,void *data);
void CGL_DeleteTexture(int textureid);
void CGL_BindTexture(int textureid);
void CGL_BindTextureChannel(int textureid,int channel);

// Draw Functionality
void CGL_DrawQuad(float x,float y,float w,float h,float u,float v,float u2,float v2);
void CGL_DrawRotatedQuad(float x,float y,float w,float h,float u,float v,float u2,float v2,float angle);

// Extended Functionality - Default Post Processing Shader (fading and scanlines)
void CGL_SetPostShader(int id);
void CGL_SetFadeValue(float fadevalue);
void CGL_SetScanlineValue(float scanlinevalue);
#endif

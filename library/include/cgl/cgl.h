// =============================================================================
// CGL - Header file
// (C) 2003-2013 Revival Studios
// Programming: Martijn Wenting
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the CML License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the CML License (CML-LICENSE.TXT) for more details.
// You should have received a copy of the CML License along with this package.
// =============================================================================
#ifndef __CGL_H__
#define __CGL_H__

#include "coreinput.h"
#include "corevideo.h"
#include "corebitmap.h"
#include "coresprite.h"
#include "corefont.h"


#define pi M_PI

typedef void (*CML_BLITTERCALLBACK)(void *dest,void *source,unsigned int size,int bpp);

#ifdef __cplusplus
extern "C" {
#endif

// --- Defines ------------------------------------------------------------------------

// Video mode flags

#define CGL_MAXBITMAPS 1024
#define CGL_MAXSPRITES 1024

// --- Typedefs --------------------------------------------------------------
typedef struct {
    int width,height,pitch;
    int bpp,pixelsize;
    unsigned int redmask,greenmask,bluemask;
    unsigned int redshift,greenshift,blueshift;
    unsigned int redbits,greenbits,bluebits;
    int blittermode,scalingmode;
    int flags;
	} s_CGLvideoinfo;

extern int coremain(int argc, char* args[]);

// --- Globals ----------------------------------------------------------------
extern s_CGLvideoinfo CGL_video;
extern bool CGL_quit;


// --- Functions --------------------------------------------------------------

// --- Video ---
extern void CGL_SetTitle(const char *title);	// Set application title
extern void CGL_ShowGraphicsMemory();
extern void CGL_FlushGraphics();
extern int *CGL_GetVideoBuffer();	// Get pointer to the current framebuffer
extern void CGL_SetRefreshRate(int fps);	// Set update speed
extern int CGL_GetFPS();	     // get FPS
extern unsigned long int CGL_GetMillis();
extern void CGL_GetRenderStatus(int *numtriangles,int *numtexturebinds,int *numstateswitches);

// ---------------------------------------------------------------------------

#ifdef __cplusplus
} // extern "C"
#endif

#endif

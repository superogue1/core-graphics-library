// micro corelib
#ifndef _CORE_LIB_
#define _CORE_LIB_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define dbg_printf printf
#define wrap_malloc malloc
#define wrap_free free
#define wrap_memset memset
#define wrap_memcpy memcpy

#endif

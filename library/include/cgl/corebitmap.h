#ifndef __COREBITMAP_H__
#define __COREBITMAP_H__

typedef struct {
	int width,height,depth;
	int texturewidth,textureheight;
	int datasize,size;
    void *surfacepointer;
    void *texturepointer;
	void *data;
	float u,v,u2,v2;
	int textureid;
	int updateflag;
	} s_bitmap;

// Bitmap functions
extern void CGL_CreateBitmap(int width,int height,s_bitmap *bitmap);
extern void CGL_DeleteBitmap(s_bitmap *bitmap);
extern void CGL_UpdateBitmap(s_bitmap bitmap);
extern void CGL_LoadBitmapStatic(char *fn,s_bitmap *bitmap);
extern void CGL_LoadBitmap(char *fn,s_bitmap *bitmap);
extern void CGL_SaveBitmap(char *fn,s_bitmap *bitmap);
extern void CGL_ClearBitmap(s_bitmap bitmap);
extern void CGL_ClearBitmapWithColor(s_bitmap bitmap,int color);

extern void CGL_DrawBitmap(int x,int y,s_bitmap src);
extern void CGL_DrawBlendBitmap(int sx,int sy,s_bitmap src,int blendmode);
extern void CGL_DrawAlphaBitmap(int sx,int sy,s_bitmap src,float alpha);

extern void CGL_DrawScaledBitmap(int sx,int sy,int w,int h,s_bitmap source,int filter);
extern void CGL_DrawZoomedBitmap(int sx,int sy,int zoom,s_bitmap source,int filter);
extern void CGL_DrawRotatedBitmap(int startx,int starty,s_bitmap src,int angle);
extern void CGL_DrawRotatedBitmapZoom(int startx,int starty,s_bitmap src,int angle,int zoom);
extern void CGL_DrawRotatedBitmapXY(int startx,int starty,s_bitmap src,int angle,int zoom,int midx,int midy);

extern void CGL_DrawBitmap3D(s_bitmap source,s_bitmap source2,int mode);

extern void CGL_DrawPixel(int x,int y,int c,s_bitmap dest);
extern int CGL_GetPixel(int x,int y,s_bitmap dest);


#endif

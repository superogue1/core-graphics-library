// =============================================================================

#ifndef __COREFONT_H__
#define __COREFONT_H__

#include <cgl/corebitmap.h>
#include <cgl/coresprite.h>

#define CGL_MAXFONTS 16

typedef struct {
        s_sprite fontchar[96];
        unsigned char fontwidth[96];
        int height;
        int numcharacters,spacing;
		s_bitmap *bitmap;
        } s_font;

extern void CGL_LoadFont(char *fn,s_font *font);
extern void CGL_LoadDarkFont(char *fn,s_font *font);

extern void CGL_SetFontSpacing(int spacing,s_font *font);
extern void CGL_DrawText(int x,int y,s_font font,char *format, ... );
extern void CGL_DrawCenteredText(int y,s_font font,char *format, ... );
extern void CGL_DrawCenteredTextXW(int x,int y,int width,s_font font,char *format, ... );
extern void CGL_DeleteFont(s_font *font);



#endif

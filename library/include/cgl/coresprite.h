#ifndef __CORESPRITE_H__
#define __CORESPRITE_H__


#define CGL_MAXSPRITES 1024

#define SPRITE_TRANSPARENCYMASK_R 0xf0
#define SPRITE_TRANSPARENCYMASK_G 0x10
#define SPRITE_TRANSPARENCYMASK_B 0xf0

#include <cgl/corebitmap.h>

typedef struct {
	int width,height,depth;
    int atlaswidth,atlasheight;
	int originx,originy;
	int hitbox_x1,hitbox_y1,hitbox_x2,hitbox_y2;
    void *surfacepointer;
    void *texturepointer;
	void *data;
	float u,v,u2,v2;
	int textureid;
	int updateflag;
	s_bitmap *bitmap;
	} s_sprite;

// Create/Delete Sprites
extern void CGL_CreateSprite(int w,int h,s_sprite *spr);
extern void CGL_LoadSprite(char *fn,s_sprite *dest);
extern void CGL_GetSprite(int sx,int sy,int w,int h,s_bitmap bitmap,s_sprite *sprite);
extern int CGL_CheckSpriteTransparency(int c);
extern s_bitmap CGL_GetBitmapFromSprite(s_sprite spr);
extern void CGL_DeleteSprite(s_sprite *sprite);
extern void CGL_SetSpriteOrigin(s_sprite *spr,int x,int y);

// Sprite Collision
extern int CGL_CheckBoxCollision(int x1,int y1,int w1,int h1,int x2,int y2,int w2,int h2);
extern int CGL_CheckSpriteCollision(int x1,int y1,s_sprite spr1,int x2,int y2,s_sprite spr2);
extern int CGL_SetSpriteCollisionBox(s_sprite *spr,int x1,int y1,int x2,int y2);

// Draw Sprites
extern void CGL_DrawSprite(int x,int y,s_sprite src);
extern void CGL_DrawMirroredSprite(int sx,int sy,s_sprite src);
extern void CGL_DrawBlendSprite(int sx,int sy,s_sprite src,int blendmode);
extern void CGL_DrawAlphaSprite(int sx,int sy,s_sprite src,float alpha);
extern void CGL_DrawScaledSprite(int sx,int sy,float w,float h,s_sprite source,int filter);
extern void CGL_DrawZoomedSprite(int sx,int sy,float zoom,s_sprite source);
extern void CGL_DrawRotatedSprite(int startx,int starty,s_sprite src,float angle);
extern void CGL_DrawRotatedSpriteZoom(int startx,int starty,s_sprite src,float angle,float zoom);
extern void CGL_DrawRotatedSpriteXY(int startx,int starty,s_sprite src,float angle,float zoom,int midx,int midy);
//extern void CGL_DrawSprite3D(int sx,int sy,int sz,s_sprite spr);

#endif

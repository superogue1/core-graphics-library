#ifndef __COREVIDEO_EXTENDED_H__
#define __COREVIDEO_EXTENDED_H__

#define CGL_VALUETYPE_FLOAT		0
#define CGL_VALUETYPE_INT		1

#define CGL_MAXSHADERS 64

// Render States
#define CGL_PROJECTIONMODE_2D	0
#define CGL_PROJECTIONMODE_3D	1

#include <cgl/corevideo.h>
#include <cgl/corebitmap.h>

typedef struct {
    	unsigned int shader_id;
    	unsigned int shader_vp;
    	unsigned int shader_fp;
	} s_shaderdata;

// Extended Functionality - Shaders
int CGL_InitShader(const char *vsText, const char *fsText);
void CGL_DeleteShader(int id);
void CGL_BindShader(int id);
void CGL_UnbindShader();
void CGL_SetShaderUniform1i(int id,char *valuename,int value);
void CGL_SetShaderUniform1f(int id,char *valuename,float value);
void CGL_SetShaderUniform2f(int id,char *valuename,float value,float value2);
void CGL_SetShaderUniform3f(int id,char *valuename,float value,float value2,float value3);
void CGL_SetShaderUniform4f(int id,char *valuename,float value,float value2,float value3,float value4);


// Extended Functionality - Basic 3D Functionality
void CGL_ViewPort(int width,int height, float aspect, float fovy, float zNear, float zFar);
void CGL_LookAt(float p_EyeX,float p_EyeY,float p_EyeZ,float p_CenterX,float p_CenterY,float p_CenterZ);
void CGL_UpdateMatrix(float *matrix);
void CGL_UpdateVertexData(float *vertex,int numvertices,int vertexformat);
void CGL_DrawTriangles(int *facelist,int numfaces);

#endif

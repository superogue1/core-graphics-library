// =============================================================================
// CML - Sound system
// (C) 2003-2007 Revival Studios
// Programming: Martijn Wenting
// =============================================================================
// Version 1.2 
//
// Date: 08-01-2007
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the CML License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the CML License (CML-LICENSE.TXT) for more details.
// You should have received a copy of the CML License along with this package.
// =============================================================================
#ifndef __CORESOUND_H__
#define __CORESOUND_H__

#define CORESOUND_MAXSOUNDS 16
#define CORESOUND_MONO 1
#define CORESOUND_STEREO 2

#define SOUNDTYPE_EFFECT  0
#define SOUNDTYPE_BGMUSIC 1


typedef int (*SNDCALLBACK)(void *stream,int size);

typedef enum {
    KHZ_8 = 8192,
    KHZ_11 = 11025,
    KHZ_22 = 22050,
    KHZ_44 = 44100
} SAMPLE_RATE;

typedef struct {
    unsigned int frequency;
    unsigned short numchannels,bits;
    unsigned int bufferlength;
    unsigned int buffersize;
    unsigned short mastervolume;
    unsigned short musicvolume;
    unsigned short effectvolume;
    
    void *mixbuffer;
    int numsounds,numactivesounds;
    int active;
	} s_soundsystem;

typedef struct {
	int id;
    unsigned short active;
    unsigned short numchannels;
    unsigned short bits;
    unsigned int size;
    unsigned int length;
    unsigned int samplerate;
    unsigned long long offset;
    unsigned int stepsize;
    unsigned int loop;
    unsigned int type;
	
	// volume/panning
    unsigned char volume,leftvolume,rightvolume;
    unsigned char panning;
    
    // streaming
    unsigned int streaming;
    unsigned int streamlength,streamsize,streampos;
    SNDCALLBACK callback;
    void *buffer;
	} s_sound;

extern s_soundsystem soundsystem;
extern s_sound *active_sounds[CORESOUND_MAXSOUNDS];

#ifdef __cplusplus
extern "C" {
#endif

extern int Soundsystem_Init(unsigned int sample_rate,unsigned char bits,unsigned char channels);
extern void Soundsystem_Closedown();

extern void Soundsystem_SetMasterVolume(int volume);
extern void Soundsystem_SetMusicVolume(int volume);
extern void Soundsystem_SetEffectVolume(int volume);
extern void Soundsystem_SetVolume(s_sound *sound,int volume);
extern void Soundsystem_SetFrequency(s_sound *sound,int freq);
extern void Soundsystem_SetPanning(s_sound *sound,int panning);

extern void Soundsystem_LoadSound(char *fn,s_sound *sound);
extern void Soundsystem_SaveSound(char *fn,s_sound *sound);
extern void Soundsystem_CreateSoundStream(s_sound *sound,void *callback,int samplerate,int bits,int numchannels,int streamlength);
extern void Soundsystem_DeleteSound(s_sound *sound);

extern void Soundsystem_PlayMusic(s_sound *sound,int looping);
extern void Soundsystem_StopMusic(s_sound *sound);
extern void Soundsystem_PlaySound(s_sound *sound,int looping);
extern void Soundsystem_StopSound(s_sound *sound);

extern int *Soundsystem_GetOutputBuffer();


#ifdef __cplusplus
} // extern "C"
#endif

#endif

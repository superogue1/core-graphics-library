// =============================================================================
// DML - Memory allocation wrapper
// (C) 2000-2007 Revival Studios
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the DML License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the DML License (DML-LICENSE.TXT) for more details.
// You should have received a copy of the DML License along with this package.
// =============================================================================

#ifndef __WRAP_MALLOC_H__
#define __WRAP_MALLOC_H__

#ifdef __cplusplus
extern "C" {
#endif

// ---------------------------------------------------------------------------

#ifdef NDEBUG

#define wrap_malloc(s)		_wrap_malloc(s)
#define wrap_malloc_iwram(s)	_wrap_malloc_iwram(s)
#define wrap_free(s)		_wrap_free(s)
#define wrap_free_iwram(s)	_wrap_fre_iwram(s)

#else

#define wrap_malloc(s)		_wrap_malloc_dbg(s,__FILE__,__LINE__)
#define wrap_malloc_iwram(s)	_wrap_malloc_iwram_dbg(s,__FILE__,__LINE__)
#define wrap_free(s)		_wrap_free_dbg(s,__FILE__,__LINE__)
#define wrap_free_iwram(s)	_wrap_free_iwram_dbg(s,__FILE__,__LINE__)

#endif

// ---------------------------------------------------------------------------

extern void * _wrap_malloc(unsigned int size);
extern void * _wrap_malloc_dbg(unsigned int size, char *file, int line);
extern void _wrap_free(void *ptr);
extern void _wrap_free_dbg(void *ptr, char *file, int line);

extern void * _wrap_malloc_iwram(unsigned int size);
extern void * _wrap_malloc_iwram_dbg(unsigned int size, char *file, int line);
extern void _wrap_free_iwram(void *ptr);
extern void _wrap_free_iwram_dbg(void *ptr, char *file, int line);

extern void wrap_memcpy(void *dst, void *src, unsigned int size);
extern void wrap_memcpy16(void *dst, void *src, unsigned int size);
extern void wrap_memcpy32(void *dst, void *src, unsigned int size);
extern void wrap_memclr(void *dst, unsigned int size);
extern void wrap_memset(void *dst, int value, unsigned int size);
extern void wrap_showusage();
// ---------------------------------------------------------------------------

#ifdef __cplusplus
} // extern "C"
#endif

#endif

#ifndef __COREFILE_H
#define __COREFILE_H

#include <stdio.h>

#define FILE_READ   0
#define FILE_WRITE  1

typedef enum
{
	MOUNT_FILE = 0,
	MOUNT_MEM = 1,
	MOUNT_DIR = 2,
	MOUNT_FAILED = 3
}MOUNT_TYPE;

typedef struct
{
	// hold the hole rom header
	void *data;
	unsigned int imagesize;
	unsigned int numfiles;

	// holds the file structure of each file in the image
	unsigned int *fileoffsets;
	char **filenames;
	unsigned int *hashnames;
}s_mountimage;

typedef struct
{
  char *displayname;
  char *filename;
  unsigned int size;
  unsigned char type;
} s_direntry; // 13bytes

typedef struct
{
	void *data;
	unsigned int fileindex;
	unsigned int offset;
	unsigned int size;
	unsigned int hashname;
}s_corefile;

#ifdef __cplusplus
extern "C" {
#endif

extern int corefile_mounttype;
extern s_mountimage corefile_image;
extern s_direntry *corefile_filelist;
extern int corefile_numfiles;

extern int corefile_mountimage(const void *image,int type);
extern void corefile_closeimage(void);
extern int readheader(void);
extern int corefile_open(char *fname,s_corefile *chunk,int flags);
extern void corefile_read(void *buffer,int size,int count,s_corefile *chunk);
extern void corefile_write(void *buffer,int size,int count,s_corefile *chunk);
extern void corefile_seek(s_corefile *chunk,unsigned int offset,int type);
extern void corefile_close(s_corefile *chunk);
extern void corefile_gets(void *buffer,int size,s_corefile *chunk);
extern int corefile_feof(s_corefile *chunk);
extern void corefile_rewind(s_corefile *chunk);
extern int corefile_tell(s_corefile *chunk);
extern int corefile_fsize(s_corefile *chunk);
extern unsigned int corefile_gethash(char *name);
extern unsigned char *corefile_loadbuffer(char *name);
extern void corefile_savebuffer(char *name,void *buffer,int size);

extern void corefile_chdir(char *path);
extern char *corefile_getpath();
extern void corefile_readdir(char *wildcard);

#ifdef __cplusplus
}
#endif


#endif

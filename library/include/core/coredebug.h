// =============================================================================
// DML - Debug functions
// (C) 2000-2007 Revival Studios
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the DML License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the DML License (DML-LICENSE.TXT) for more details.
// You should have received a copy of the DML License along with this package.
// =============================================================================

#ifndef	__COREDEBUG_H__
#define	__COREDEBUG_H__

#ifdef __cplusplus
extern "C" {
#endif

// ---------------------------------------------------------------------------

#ifdef NDEBUG

#define dbg_init()			((void) 0)
#define dbg_shutdown()			((void) 0)
#ifdef _MSC_VER
#define dbg_printf()			((void) 0)
#else
#define dbg_printf(format, args...)	((void) 0)
#endif
#define dbg_assert(e)			((void) 0)

// ---------------------------------------------------------------------------

#else // NDEBUG

#define dbg_init()			_dbg_init()
#define dbg_shutdown()			_dbg_shutdown()
#ifdef _MSC_VER
#define dbg_printf			_dbg_printf
#else
#define dbg_printf(format, args...)	_dbg_printf(format, ##args)
#endif

#ifdef NOASSERT
#define dbg_assert(e)			((void) 0)
#else
#define dbg_assert(e)			((e) ? ((void) 0) : _dbg_assert(__FILE__, __LINE__, #e))
#endif

extern void error(char *msg);
extern void _dbg_init();
extern void _dbg_shutdown();
extern void _dbg_printf(char *format, ...);
extern void _dbg_assert(char *file, int line, char *error);

#endif // NDEBUG

// ---------------------------------------------------------------------------

#ifdef __cplusplus
} // extern "C"
#endif

#endif


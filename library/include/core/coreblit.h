// =============================================================================
// Core - Blitter routines
// (C) 2003-2007 Revival Studios
// Programming: Martijn Wenting
// =============================================================================
// Version 1.2 
//
// Date: 08-01-2007
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the CML License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the CML License (CML-LICENSE.TXT) for more details.
// You should have received a copy of the CML License along with this package.
// =============================================================================

#ifndef __COREBLIT_H__
#define __COREBLIT_H__

#ifdef __cplusplus
extern "C" {
#endif

#define COREBLIT_ARGB8888       0
#define COREBLIT_ABGR8888       1
#define COREBLIT_ARGB1555       2
#define COREBLIT_ABGR1555       3
#define COREBLIT_RGB565         4
#define COREBLIT_BGR565         5
#define COREBLIT_RGB556         6
#define COREBLIT_BGR556         7

#ifndef u8
#define u8 unsigned char
#endif
#ifndef u16
#define u16 unsigned short
#endif
#ifndef u32
#define u32 unsigned int
#endif

#define BLIT_BOUNDMAX(x, high) ( ((x) > (high)) ? (high) : (x) )


extern int coreblit_redbits,coreblit_greenbits,coreblit_bluebits;
extern int coreblit_redshift,coreblit_greenshift,coreblit_blueshift;
extern int coreblit_redmask,coreblit_greenmask,coreblit_bluemask;

extern void coreblit_initialise(int rbits,int gbits,int bbits,int rshift,int gshift,int bshift);

// Color functions
extern unsigned short CoreBlit_color24to16(int c);
extern int CoreBlit_color16to24(unsigned short c);

// Blitter functions
extern void CoreBlit_BlitPixel32(u32 *dest,u32 c);
extern u32 CoreBlit_BlitGetPixel32(u32 *dest);

extern void CoreBlit_BlitFill32(u32 *dest,u32 size,u32 c);
extern void CoreBlit_BlitFillShade32(u32 *dst,u32 length,u32 c,int addc);
extern void CoreBlit_BlitFillTexture32(u32 *dst,u32 *src,u32 length,int u,int v,int addu,int addv);
extern void CoreBlit_BlitFillShadeTexture32(u32 *dst,u32 *src,u32 length,int u,int v,int c,int addu,int addv,int addc);
extern void CoreBlit_BlitFillDualTexture32(u32 *dst,u32 *src,u32 *src2,u32 length,int u,int v,int u2,int v2,int addu,int addv,int addu2,int addv2);
extern void CoreBlit_BlitStretch32(u32 *dst,u32 *src,u32 length,int u,int ustep);
extern void CoreBlit_BlitBiLinearStretch32(u32 *dst,u32 *src,u32 length,int sw,int u,int v,int ustep);
extern void CoreBlit_BlitTranspStretch32(u32 *dst,u32 *src,u32 length,int u,int ustep);

extern void CoreBlit_BlitCopy32(u32 *dest,u32 *source,u32 size);
extern void CoreBlit_BlitInvert32(u32 *dest,u32 *source1,u32 size);
extern void CoreBlit_BlitColorize32(u32 *dest,u32 *src,u32 c,u32 size);
extern void CoreBlit_BlitMix32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitQMix32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitAdd32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitMul32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitDarken32(u32 *dest,u32 *source1,u32 size);
extern void CoreBlit_BlitBrighten32(u32 *dest,u32 *source1,u32 size);
extern void CoreBlit_BlitBlur32(u32 *dest,u32 *source,int ofs1,int ofs2,int length);
extern void CoreBlit_BlitAntiAlias32(u32 *dst,u32 *src,int ofs1,int ofs2,int length);

extern void CoreBlit_BlitFade32(u32 *dest,u32 *source,u8 fadestep,u32 size);
extern void CoreBlit_BlitFadeWhite32(u32 *dest,u32 *source,u8 fadestep,u32 size);
extern void CoreBlit_BlitCrossfade32(u32 *dest,u32 *source1,u32 *source2,u8 fadestep,u32 size);
extern void CoreBlit_BlitAlphawipe32(u32 *dest,u32 *source1,u32 *source2,u8 fadestep,u32 size);

extern void CoreBlit_BlitTransp32(u32 *dest,u32 *source,u32 size);
extern void CoreBlit_BlitTranspMix32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitTranspQMix32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitTranspQ3Mix32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitTranspAdd32(u32 *dest,u32 *source1,u32 *source2,u32 size);
extern void CoreBlit_BlitTranspMul32(u32 *dest,u32 *source1,u32 *source2,u32 size);


// 16-bit
extern void CoreBlit_BlitPixel16(u16 *dest,u32 c);
extern u32 CML_GetPixel16(u16 *dest);

extern void CoreBlit_BlitFill16(u16 *dest,u32 size,u32 c);
extern void CoreBlit_BlitFillShade16(u16 *dst,u32 length,u32 c,int addc);
extern void CoreBlit_BlitFillTexture16(u16 *dst,u16 *src,u32 length,int u,int v,int addu,int addv);
extern void CoreBlit_BlitFillShadeTexture16(u16 *dst,u16 *src,u32 length,int u,int v,int c,int addu,int addv,int addc);
extern void CoreBlit_BlitFillDualTexture16(u32 *dst,u32 *src,u32 *src2,u32 length,int u,int v,int u2,int v2,int addu,int addv,int addu2,int addv2);
extern void CoreBlit_BlitStretch16(u16 *dst,u16 *src,u32 length,int u,int ustep);
extern void CoreBlit_BlitBiLinearStretch16(u16 *dst,u16 *src,u32 length,int sw,int u,int v,int ustep);
extern void CoreBlit_BlitTranspStretch16(u16 *dst,u16 *src,u32 length,int u,int ustep);

extern void CoreBlit_BlitCopy16(u16 *dest,u16 *source,u32 size);
extern void CoreBlit_BlitInvert16(u16 *dest,u16 *source1,u32 size);
extern void CoreBlit_BlitColorize16(u16 *dest,u16 *src,u32 c,u32 size);
extern void CoreBlit_BlitMix16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitQMix16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitAdd16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitMul16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitDarken16(u16 *dest,u16 *source1,u32 size);
extern void CoreBlit_BlitBrighten16(u16 *dest,u16 *source1,u32 size);
extern void CoreBlit_BlitBlur16(u16 *dest,u16 *source,int ofs1,int ofs2,int length);
extern void CoreBlit_BlitAntiAlias16(u16 *dst,u16 *src,int ofs1,int ofs2,int length);

extern void CoreBlit_BlitFade16(u16 *dest,u16 *source,u8 fadestep,u32 size);
extern void CoreBlit_BlitFadeWhite16(u16 *dest,u16 *source,u8 fadestep,u32 size);
extern void CoreBlit_BlitCrossfade16(u16 *dest,u16 *source1,u16 *source2,u8 fadestep,u32 size);
extern void CoreBlit_BlitAlphawipe16(u16 *dest,u16 *source1,u16 *source2,u8 fadestep,u32 size);

extern void CoreBlit_BlitTransp16(u16 *dest,u16 *source,u32 size);
extern void CoreBlit_BlitTranspMix16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitTranspQMix16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitTranspQ3Mix16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitTranspAdd16(u16 *dest,u16 *source1,u16 *source2,u32 size);
extern void CoreBlit_BlitTranspMul16(u16 *dest,u16 *source1,u16 *source2,u32 size);


#ifdef __cplusplus
} // extern "C"
#endif

#endif

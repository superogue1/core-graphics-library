// =============================================================================
// CML Extension - Resource browser
// (C) 2006-2008 Revival Studios
// Programming: Martijn Wenting
// =============================================================================
// This program is free software; you can redistribute it and use it under the
// terms of the CML License. This program is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the CML License (CML-LICENSE.TXT) for more details.
// You should have received a copy of the CML License along with this package.
// =============================================================================

#ifndef __RESBROWSER_H__
#define __RESBROWSER_H__

#include <coremedia/cml.h>

#ifdef __cplusplus
extern "C" {
#endif

extern char *BrowseResource(char *wildcard,s_bitmap bgbmp,s_font browsefont,s_font browsefont2);
extern int resbrowser_lastchoice;

#ifdef __cplusplus
}
#endif

#endif

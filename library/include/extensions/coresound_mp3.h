// =============================================================================
// CGL Extension - MP3 Streaming module
//
// Based on MINIMP3:
// Copyright (c) 2001, 2002 Fabrice Bellard,
// (c) 2007 Martin J. Fiedler 
// =============================================================================
// Version 1.0
// =============================================================================
#ifndef	__MP3CORE_H__
#define	__MP3CORE_H__

#ifdef __cplusplus
extern "C" {
#endif

extern void Soundsystem_InitMP3Stream(char *fn,s_sound *sound);
extern void Soundsystem_CloseMP3Stream();

#ifdef __cplusplus
} // extern "C"
#endif

#endif


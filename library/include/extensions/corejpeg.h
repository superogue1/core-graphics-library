// =============================================================================
// CGL extension - JPEG Loader
// 
// Uses libjpeg - (c) Independent JPEG Group
// =============================================================================

#ifndef __LOADJPEG_H__
#define __LOADJPEG_H__

#include <cgl/cgl.h>

extern void CGL_LoadBitmapJPEG(char *fn,s_bitmap *image);

#endif

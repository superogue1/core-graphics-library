// =============================================================================
// CGL extension - PNG Loader
// 
// Uses LodePNG , Copyright (c) 2005-2015 Lode Vandevenne
// =============================================================================

#ifndef __CORE_LOADPNG_H__
#define __CORE_LOADPNG_H__

#include <cgl/cgl.h>

void CGL_LoadBitmapPNG(char *fn,s_bitmap *image);

#endif
